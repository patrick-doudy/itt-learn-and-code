/*
    Patrick Doudy

    Spider Queue assignment from learn and code session
    of 3/27/2018

    https://www.hackerearth.com/practice/data-structures/queues/basics-of-queues/practice-problems/algorithm/monk-and-chamber-of-secrets/

*/

#include <stdio.h>

struct spider_struct{
    int power;
    int pos;
};

typedef struct spider_struct spider_t;

int main(){
    
    // state variables and problem data structures
    // startup and input gathering
    // ===========================================
    int total_spiders, desired_spiders, chunk_size, num_temp, spiders_enqueued;
    int spiders_selected = 0;

    scanf("%d %d", &total_spiders, &chunk_size);
    desired_spiders = chunk_size;
    
    spider_t spiders_queue[total_spiders];
    spider_t spiders_out[total_spiders];
    spider_t spiders_temp[chunk_size];

    int idx = 0;
    for(; idx < total_spiders; ++idx){
        spider_t * spider = spiders_queue + idx;
        spider->pos = idx+1;
        scanf("%d",&(spider->power));
    }
    spiders_enqueued = total_spiders;
    
    // function definitions
    // ====================
    void copy_spider(spider_t * dest, int d_idx, spider_t * src, int s_idx){
        (dest +d_idx)->power = (src +s_idx)->power;
        (dest +d_idx)->pos   = (src +s_idx)->pos;
    }
    
    void slide_queue(int count){
        int idx = 0;
        for(; (idx +count) < total_spiders; ++idx)
            copy_spider(spiders_queue, idx, spiders_queue, idx+count);
    }
    
    void deque_n_spiders(int n){
        int count = (n <= spiders_enqueued)
                  ?  n
                  :  spiders_enqueued;
        int idx = num_temp = 0;

        for(; idx < count; ++idx, ++num_temp, --spiders_enqueued)
            copy_spider(spiders_temp, idx, spiders_queue, idx);
        
        slide_queue(count);
    }

    int max_power_idx(void){
        int max_power   = -1;
        int select_idx  = -1;
        int idx = 0;
        spider_t * spider;

        for(; idx < num_temp; ++idx){
            spider = spiders_temp + idx;
            if(spider->power > max_power){
                max_power   = spider->power;
                select_idx  = idx;
            }
        }

        return select_idx;
    }
    
    void move_iteration_spider(int select_idx){
        if(select_idx >= 0)
            copy_spider(spiders_out, spiders_selected++, spiders_temp, select_idx);
    }

    void shuffle_remaining_spiders(int select_idx){
        spider_t * spider;
        int idx = 0;
        for(; idx < num_temp; ++idx){
            if(idx == select_idx)
                continue;

            spider = spiders_temp + idx;

            if(spider->power > 0)
                --(spider->power);

            copy_spider(spiders_queue, spiders_enqueued++, spiders_temp, idx);
        }
    }

    // debugging assistance
    // ====================
    void print_spider_queue(void){
        int idx = 0;
        spider_t * spider;
        printf("queue: ");
        for(; idx < spiders_enqueued; ++idx){
            spider = spiders_queue + idx;
            printf("%d ", spider->power);
        }
        printf("\n");
    }
    
    void print_selected_spiders(void){
        int idx = 0;
        spider_t * spider;
        printf("selected (pow, pos): ");
        for(; idx < spiders_selected; ++idx){
            spider = spiders_out + idx;
            printf("(%d, %d) ", spider->power, spider->pos);
        }
        printf("\n");
    }

    // program driver
    // ==============
    while(spiders_selected < desired_spiders){
        int select_idx = -1;
        deque_n_spiders(chunk_size);
        select_idx = max_power_idx();
        move_iteration_spider(select_idx);
        shuffle_remaining_spiders(select_idx);
    }

    // program output and termination
    // ==============================
    for(idx = 0; idx < spiders_selected; ++idx){
        spider_t * spider = spiders_out + idx;
        printf("%d ",spider->pos);
    }
    printf("\n");

    return 0;
}