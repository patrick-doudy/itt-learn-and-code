package com.itt.tds.test;

import java.util.*;

import com.itt.tds.core.*;
import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.repository.*;
import com.itt.tds.coordinator.Client;

public class TestUtil {
	
	private static final String helloWorldByteCode = (
		"cafe babe 0000 0034 001d 0a00 0600 0f09"
		+"0010 0011 0800 120a 0013 0014 0700 1507"
		+"0016 0100 063c 696e 6974 3e01 0003 2829"
		+"5601 0004 436f 6465 0100 0f4c 696e 654e"
		+"756d 6265 7254 6162 6c65 0100 046d 6169"
		+"6e01 0016 285b 4c6a 6176 612f 6c61 6e67"
		+"2f53 7472 696e 673b 2956 0100 0a53 6f75"
		+"7263 6546 696c 6501 000f 4865 6c6c 6f57"
		+"6f72 6c64 2e6a 6176 610c 0007 0008 0700"
		+"170c 0018 0019 0100 0b48 656c 6c6f 2057"
		+"6f72 6c64 0700 1a0c 001b 001c 0100 0a48"
		+"656c 6c6f 576f 726c 6401 0010 6a61 7661"
		+"2f6c 616e 672f 4f62 6a65 6374 0100 106a"
		+"6176 612f 6c61 6e67 2f53 7973 7465 6d01"
		+"0003 6f75 7401 0015 4c6a 6176 612f 696f"
		+"2f50 7269 6e74 5374 7265 616d 3b01 0013"
		+"6a61 7661 2f69 6f2f 5072 696e 7453 7472"
		+"6561 6d01 0007 7072 696e 746c 6e01 0015"
		+"284c 6a61 7661 2f6c 616e 672f 5374 7269"
		+"6e67 3b29 5600 2100 0500 0600 0000 0000"
		+"0200 0100 0700 0800 0100 0900 0000 1d00"
		+"0100 0100 0000 052a b700 01b1 0000 0001"
		+"000a 0000 0006 0001 0000 0002 0009 000b"
		+"000c 0001 0009 0000 0025 0002 0001 0000"
		+"0009 b200 0212 03b6 0004 b100 0000 0100"
		+"0a00 0000 0a00 0200 0000 0600 0800 0700"
		+"0100 0d00 0000 0200 0e").replace(" ","");

	private static Random random = new Random();

	private static byte hexToByte(String hex) {

		int dig1 = toDigit(hex.charAt(0));
		int dig2 = toDigit(hex.charAt(1));

		return (byte) ((dig1 << 4) + dig2);
	}
	 
	private static int toDigit(char hex) {

		int dig = Character.digit(hex, 16);

		if(dig >= 0) return dig;

		throw new IllegalArgumentException(
			"Invalid Hex Char: " + hex
		);
	}

	private static byte[] decodeHexString(String hex) {

		if(hex.length() % 2 != 0)
			throw new IllegalArgumentException(
				"Invalid Hex String: " + hex
			);

		byte[] bytes = new byte[hex.length() / 2];

		for(int idx = 0; idx < hex.length(); idx += 2)
			bytes[idx / 2] = hexToByte(hex.substring(idx, idx + 2));

		return bytes;
	}

	public static byte[] getHelloWorld() {

		return decodeHexString(helloWorldByteCode);
	}

	public static int randomPort() {

		int lower = 49152;
		int upper = 65535;
		int range = upper -lower;

		return (lower + random.nextInt(range));
	}

	public static String randomString(int len) {

		int lower = 97;  // 'a'
		int upper = 122; // 'z'
		int range = upper -lower;

		StringBuilder builder = new StringBuilder(len);

		do {

			builder.append((char) (lower + random.nextInt(range)));

		} while(builder.length() < len);

		return builder.toString();
	}

	public static String randomJsonPair() {

		String json = "{\"" + randomString(16) + "\":\"" + randomString(16) + "\"}";

		return json;
	}

	public static String randomIp() {

		String str = "";

		for(int idx = 0; idx < 4; ++idx){
			str += Integer.toString(random.nextInt(255));

			if(idx < 3)
				str += ".";
		}

		return str;
	}

	public static Node randomNode() throws RepositoryException {

		String 		nodeIp 		= TestUtil.randomIp();
		int 		nodePort  	= TestUtil.randomPort();
		NodeState	nodeState 	= NodeState.AVAILABLE;

		while(Repositories.getNodeRepository().resolveNodeId(
				nodeIp,
				nodePort
				) > 0
			){

			nodeIp 		= TestUtil.randomIp();
			nodePort 	= TestUtil.randomPort();
		}

		return new Node(nodeIp, nodePort, nodeState);		
	}

	public static Client randomClient() throws RepositoryException {

		String hostName = TestUtil.randomString(16);
		String userName = TestUtil.randomString(16);

		while(Repositories.getClientRepository().resolveClientId(
				hostName,
				userName
				) > 0
			){

			hostName = TestUtil.randomString(16);
			userName = TestUtil.randomString(16);
		}

		return new Client(hostName, userName);		
	}

	public static Task randomTask(int clientId) {

		String taskName 		= randomString(16);
		String taskParameters	= randomJsonPair();

		byte[] taskBytes 		= new byte[16];

		random.nextBytes(taskBytes);

		return new Task(
			taskName,
			taskParameters,
			clientId,
			taskBytes
		);
	}

	public static TaskResult randomResult(int taskId) {

		byte[] resultBuff = new byte[16];

		random.nextBytes(resultBuff);

		return new TaskResult(
			200,
			"Success",
			resultBuff,
			taskId,
			TaskOutcome.SUCCESS
		);
	}
}