package com.itt.tds.test;

import static org.junit.Assert.*;
import org.junit.*;

import java.io.*;
import java.util.*;

import com.itt.tds.coordinator.CommunicationManager;
import com.itt.tds.coordinator.TaskScheduler;
import com.itt.tds.coordinator.Client;
import com.itt.tds.coordinator.db.repository.*;
import com.itt.tds.comm.*;
import com.itt.tds.core.Task;

public class TestTaskScheduler {

	private static CommunicationManager commMgr 
		 = new CommunicationManager();

	private static List<com.itt.tds.coordinator.Node> getAvailableNodes()
		throws RepositoryException {

		return Repositories.getNodeRepository().getAvailableNodes();
	}

	@BeforeClass
	public static void initialize() throws IOException {

		commMgr.openListener();
	}

	@AfterClass
	public static void shutDown() throws IOException {

		/*
			node spawns execution thread for task, for clean
			exit need to wait until this thread completes
			before shutting down communication manager
		*/

		Set<Thread> threads = Thread.getAllStackTraces().keySet();

		for(Thread thread : threads)
			if(thread
				.getName()
				.equals(
					com.itt.tds.node.Node.TASK_THREAD_NAME)
			){

				try {

					thread.join();

				} catch(InterruptedException e){

					// allow to die //
				}
			}

		commMgr.stopListener();
	}

	@Test
	public void testAssignTaskToNode()
		throws IOException, RepositoryException {

		// clear any dummy "available" nodes from repository //

		List<com.itt.tds.coordinator.Node> nodes = getAvailableNodes();

		while(!nodes.isEmpty()){

			Repositories
			.getNodeRepository()
			.delete(nodes.get(0));

			nodes.remove(0);
		}

		com.itt.tds.node.Node node = 
			new com.itt.tds.node.Node();

		node.startUp();

		// await completion of node startup //

		while(!node.listening());

		// await registration of worker node //

		nodes = getAvailableNodes();

		while(nodes.isEmpty()){

			nodes = getAvailableNodes();
		}

		// arrange requisite data //

		com.itt.tds.coordinator.Node coordRepOfNode = nodes.get(0);

		Client 	client 		= TestUtil.randomClient();
		int 	clientId 	= Repositories.getClientRepository().add(client);

		Task helloWorld = new Task(
			"HelloWorld",
			"{ \""+Task.ARGSKEY+"\" : \"\" }",
			clientId,
			TestUtil.getHelloWorld()
		);

		helloWorld.setId(Repositories.getTaskRepository().add(helloWorld));

		TaskScheduler scheduler = new TaskScheduler();

		// submit delegation //

		TDSResponse response = scheduler.assignTaskToNode(
			helloWorld,
			coordRepOfNode
		);

		// release node thread //

		node.shutDown();

		assertEquals("Success", response.getErrorMessage());
		assertEquals(200, response.getErrorCode());
		assertTrue(response.getStatus());
	}
}