package com.itt.tds.test;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;

import java.io.*;
import java.net.*;
import java.util.*;
import java.nio.charset.StandardCharsets;

import com.itt.tds.cfg.*;
import com.itt.tds.node.*;
import com.itt.tds.core.*;
import com.itt.tds.comm.*;
import com.itt.tds.coordinator.Client;
import com.itt.tds.coordinator.db.repository.*;
import com.itt.tds.coordinator.CommunicationManager;

public class TestNode {

	private static TDSSerializer serializer
		= TDSSerializerFactory.getSerializer();

	private static CommunicationManager commMgr 
		= new CommunicationManager();

	private static boolean taskThreadSeen = false;

	@BeforeClass
	public static void initialize() throws IOException {

		commMgr.openListener();
	}

	@AfterClass
	public static void shutDown() throws IOException {

		/*
			node spawns execution thread for task, for clean
			exit need to wait until this thread completes
			before shutting down communication manager
		*/

		Set<Thread> threads = Thread.getAllStackTraces().keySet();

		for(Thread thread : threads)
			if(thread
				.getName()
				.equals(
					com.itt.tds.node.Node.TASK_THREAD_NAME)
			){

				try {

					thread.join();

				} catch(InterruptedException e){

					// allow to die //
				}
			}

		commMgr.stopListener();
	}

	@Test
	public void testExecuteTask() {

		Node node = new Node();

		Task helloWorld = new Task(
			"HelloWorld",
			"{ \""+Task.ARGSKEY+"\" : \"\" }",
			TestUtil.getHelloWorld()
		);

		TaskResult result = node.executeTask(helloWorld);

		assertEquals("", result.errorMessage);
		assertEquals(0, result.errorCode);
		assertEquals(TaskOutcome.SUCCESS, result.taskOutcome);
		assertEquals("Hello World", new String(result.resultBuffer));
	}

	@Test
	public void testRequest() {

		commMgr.setMockResponse(TDSResponse.getMockResponse());

		Node node = new Node();

		TDSResponse compareResponse = TDSResponse.getMockResponse();
		TDSRequest  inRequest 		= TDSRequest.getMockRequest();

		TDSResponse outResponse 	= node.sendRequest(inRequest);

		assertTrue(compareResponse.equals(outResponse));

		commMgr.unsetMockResponse();
	}

	@Test
	public void testPostResult() throws RepositoryException {

		// Result requires task and client in place

		Client 	client 		= TestUtil.randomClient();
		int 	clientId  	= Repositories.getClientRepository().add(client);
		Task 	task 	  	= TestUtil.randomTask(clientId);
		int 	taskId 	  	= Repositories.getTaskRepository().add(task);

		Node node = new Node();

		TaskResult result 	 = TestUtil.randomResult(taskId);

		TDSResponse response = node.postResult(result);

		assertEquals(200, response.getErrorCode());
		assertTrue(response.getStatus());
	}

	@Test
	public void testRegisterNode() {

		Node node = new Node();

		TDSResponse response = node.registerNode();

		assertEquals("Success", response.getErrorMessage());
		assertEquals(200, response.getErrorCode());
		assertTrue(response.getStatus());
	}

	@Test
	public void testStartUpShutDown() throws IOException {

		Node node = new Node();

		node.startUp();

		assertTrue(node.isAvailable());

		node.shutDown();

		assertEquals(NodeState.NOT_OPERATIONAL, node.getState());
	}

	@Test
	public void testTaskReceive()
		throws IOException,
		RepositoryException,
		UnknownHostException,
		InterruptedException {

		Node node = new Node();

		node.startUp();

		Client 	client 		= TestUtil.randomClient();
		int 	clientId  	= Repositories.getClientRepository().add(client);

		Task helloWorld 	= new Task(
			"HelloWorld",
			"{ \""+Task.ARGSKEY+"\" : \"\" }",
			clientId,
			TestUtil.getHelloWorld()
		);

		int 	taskId 	  	= Repositories.getTaskRepository().add(helloWorld);

		Socket socket = new Socket(
			"127.0.0.1",
			node.getListenPort()
		);

		BufferedReader socketIn = new BufferedReader(
									new InputStreamReader(
										socket.getInputStream()
									)
								);

		PrintWriter socketOut 	= new PrintWriter(
									socket.getOutputStream(),
									true
								);

		TDSRequest request = new TDSRequest("task-assign");

		request.setSourceIp(socket.getLocalAddress().getHostAddress());
		request.setSourcePort(socket.getLocalPort());
		request.setDestIp("127.0.0.1");
		request.setDestPort(node.getListenPort());

		request.addParameter("taskId", Integer.toString(taskId));
		request.addParameter("taskName", helloWorld.getTaskName());
		request.addParameter("taskParameters", helloWorld.getTaskParameters());
		request.setData(helloWorld.getProgramBytes());

		socketOut.println(serializer.serialize(request));

		TDSResponse response = (TDSResponse)
			serializer.deserialize(socketIn.readLine());

		socket.close();

		node.shutDown();

		assertTrue(TDSResponse
			.goodRequest(request)
			.equals(response)
		);
	}
}