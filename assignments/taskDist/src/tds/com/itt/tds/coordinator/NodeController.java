package com.itt.tds.coordinator;

import java.util.*;

import com.itt.tds.comm.*;
import com.itt.tds.core.*;
import com.itt.tds.coordinator.db.repository.*;

public class NodeController implements TDSController {

    public NodeController() {

    }

    @Override
    public TDSResponse processRequest(TDSRequest request){

        String method = request.getMethod();

        TDSResponse response = null;

        if(method.equals("node-add")
        || method.equals("node-result")){

            if(method.equals("node-add"))
                response = registerNode(request);

            if(method.equals("node-result"))
                response = saveResult(request);

        } else {

            response = TDSResponse.badRequest(request);
            response.setErrorMessage("no such method");
        }

        return response;
    }

    private TDSResponse registerNode(TDSRequest request) {
        
        Node node = null;

        try {

            node = new Node(
                request.getSourceIp(), 
                Integer.parseInt(request.getParameter("nodeListenPort")),
                NodeState.fromString(request.getParameter("nodeState"))
            );

        } catch (Exception e) {

            TDSResponse badRequest = TDSResponse.badRequest(request);
            badRequest.setErrorMessage(e.getMessage());

            return badRequest;      
        }

        try {

            Repositories.getNodeRepository().add(node);
            
            return TDSResponse.goodRequest(request);

        } catch (RepositoryException e) {

            TDSResponse errResponse = TDSResponse.serverError(request);
            errResponse.setErrorMessage(e.getMessage());

            return errResponse;
        }
    }

    private TDSResponse saveResult(TDSRequest request) {
        
        TaskResult taskResult = null;

        try {

            taskResult = new TaskResult(
                Integer.parseInt(request.getParameter("errorCode")),
                request.getParameter("errorMessage"),
                request.getData(),
                Integer.parseInt(request.getParameter("taskId")),
                TaskOutcome.fromString(request.getParameter("taskOutcome"))
            );

        } catch (Exception e) {

            TDSResponse badRequest = TDSResponse.badRequest(request);
            badRequest.setErrorMessage(e.getMessage());

            return badRequest;
        }

        try {     

            int listenPort  = Integer.parseInt(request.getParameter("nodeListenPort"));

            Node node   = null;
            Task task   = null;

            int nodeId = Repositories
                .getNodeRepository()
                .resolveNodeId(
                    request.getSourceIp(),
                    listenPort
                );

            node = Repositories
                .getNodeRepository()
                .getNodeById(nodeId);

            task = Repositories
                .getTaskRepository()
                .getTaskById(taskResult.taskId);

            if(node != null){
                node.setState(NodeState.AVAILABLE);
                Repositories.getNodeRepository().modify(node);
            }

            if(task != null){
                task.setTaskState(TaskState.COMPLETED);
                Repositories.getTaskRepository().modify(task);
            }

            Repositories.getTaskResultRepository().add(taskResult);

            return TDSResponse.goodRequest(request);

        } catch (RepositoryException e) {

            TDSResponse errResponse = TDSResponse.serverError(request);
            errResponse.setErrorMessage(e.getMessage());

            return errResponse;
        }
    }
}