package com.itt.tds.coordinator;

import java.util.*;
import java.io.*;
import java.net.*;

import com.itt.tds.core.*;
import com.itt.tds.comm.*;
import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.repository.*;

public class TaskScheduler {

    private static TDSSerializer serializer = TDSSerializerFactory.getSerializer();

    public static synchronized void taskScan() {

        List<Task> tasks = null;
        List<Node> nodes = null;

        try {

            tasks = Repositories
                .getTaskRepository()
                .getTasksByStatus(TaskState.PENDING);

            nodes = Repositories
                .getNodeRepository()
                .getAvailableNodes();

        } catch (RepositoryException e){

            // nothing to be done, terminate
            // and await next scan

            return;
        }

        // dispatch requests //

        while(!tasks.isEmpty() && !nodes.isEmpty()){

            Task task = tasks.get(0);
            Node node = nodes.get(0);

            TDSResponse response = assignTaskToNode(task, node);

            if(response.getStatus()
            && response.getErrorCode() == 200
            && response.getErrorMessage().equals("Success")){

                task.setTaskState(TaskState.IN_PROGRESS);
                node.setState(NodeState.BUSY);

                try {

                    Repositories.getNodeRepository().modify(node);
                    Repositories.getTaskRepository().modify(task);

                } catch (RepositoryException e){

                    // nothing to be done if we can't modify DB //
                
                } finally {

                    tasks.remove(0);
                    nodes.remove(0);
                }

            } else if(response.getErrorMessage().equals("Unavailable")){

                node.setState(NodeState.BUSY);

                try {

                    Repositories.getNodeRepository().modify(node);

                } catch (RepositoryException e){

                    // nothing to be done if we can't modify DB //

                } finally {

                    nodes.remove(0);
                }

            } else {

                node.setState(NodeState.NOT_OPERATIONAL);

                try {

                    Repositories.getNodeRepository().modify(node);

                } catch (RepositoryException e){

                    // nothing to be done if we can't modify DB //

                } finally {

                    nodes.remove(0);
                }
            }


        }
    }

    public static TDSResponse assignTaskToNode(Task task, Node node){

        TDSRequest request = new TDSRequest("task-assign");

        request.addParameter("taskId", task.getId());
        request.addParameter("taskName", task.getTaskName());
        request.addParameter("taskParameters", task.getTaskParameters());
        request.setData(task.getProgramBytes());

        return sendNodeRequest(request, node);
    }

    private static TDSResponse sendNodeRequest(TDSRequest request, Node node){

        TDSResponse     response    = null;
        Socket          socket      = null;
        BufferedReader  socketIn    = null;
        PrintWriter     socketOut   = null;

        try {

            socket = new Socket();
            socket.connect(
                new InetSocketAddress(node.getIp(), node.getPort()),
                10
            );

            socketIn    = new BufferedReader(
                            new InputStreamReader(
                                socket.getInputStream()
                            )
                        );

            socketOut   = new PrintWriter(
                            socket.getOutputStream(),
                            true
                        );

            request.setSourceIp(socket.getLocalAddress().getHostAddress());
            request.setSourcePort(socket.getLocalPort());
            request.setDestIp(node.getIp());
            request.setDestPort(node.getPort());

            socketOut.println(serializer.serialize(request));

            response = (TDSResponse)
                serializer.deserialize(socketIn.readLine());

            socket.close();

        } catch (Exception e) {

            response = TDSResponse.badRequest(request);
            response.setErrorMessage(
                e.getClass().getName() + " : " + e.getMessage()
            );
        }

        return response;
    }
}