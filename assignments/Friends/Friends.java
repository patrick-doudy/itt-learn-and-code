/*

	Hacker Earth Remove Friends Problem
	ITT learn and code 2018

	Patrick Doudy May 2nd 2018

*/

import java.io.InputStream;
import java.util.Scanner;
import java.util.ArrayList;

class Friends {
	public static void main(String[] Args){
		FriendsCuller friendsCuller = new FriendsCuller(System.in);
	}
}

class FriendsCuller {

	private Scanner scanner;

	private ArrayList<TestCase> tests;

	// Constructor & Driver //

	public FriendsCuller(InputStream inputStream){

		tests = new ArrayList<TestCase>();

		scanner = new Scanner(inputStream);

		int numTests = scanner.nextInt();

		for(int t = 0; t < numTests; ++t)
			tests.add(new TestCase(scanner));

		for(int t = 0; t < tests.size(); ++t)
			handleTest(tests.get(t));
	}

	// Solution Operations //

	private void handleTest(TestCase test){
		cull(test);
		test.printFriends();
	}

	private void cull(TestCase test){

		int friendsInitial  = test.getFriendsInitial();
		int friendsToRemove = test.getFriendsToRemove();

		ArrayList<Friend> friends = test.getFriends();

		for(int removed = 0; removed < friendsToRemove; ++removed){

			boolean found = false;

			for(int i = 0; i < friends.size(); ++i){
				if(friends.get(i).getPopularity() < friends.get(i+1).getPopularity()){
					friends.remove(i);
					found = true;
					break;
				}
			}

			if(!found)
				removeLeastPopular(friends);
		}
	}

	private void removeLeastPopular(ArrayList<Friend> friends){

		int idxMin = 0;

		for(int f = 0; f < friends.size(); ++f)
			if(friends.get(f).getPopularity() < friends.get(idxMin).getPopularity())
				idxMin = f;

		friends.remove(idxMin);
	}

	// Helper Classes //

	public class Friend {

		private int popularity;

		public Friend(int popularity){
			this.popularity = popularity;
		}

		public int getPopularity(){
			return popularity;
		}
	}

	public class TestCase{

		private int friendsInitial;
		private int friendsToRemove;

		private ArrayList<Friend> friends;

		public TestCase(Scanner scanner){

			friends = new ArrayList<Friend>();

			friendsInitial  = scanner.nextInt();
			friendsToRemove = scanner.nextInt();

			for(int f = 0; f < friendsInitial; ++f)
				friends.add(new Friend(scanner.nextInt()));
		}

		public int getFriendsInitial(){
			return friendsInitial;
		}

		public int getFriendsToRemove(){
			return friendsToRemove;
		}

		public ArrayList<Friend> getFriends(){
			return friends;
		}

		public void printFriends(){
			for(int f = 0; f < friends.size(); ++f)
				System.out.print(friends.get(f).getPopularity()+" ");

			System.out.println();
		}
	}
}